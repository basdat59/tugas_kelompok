from django.db import connection
from django.shortcuts import redirect, render
from rs_cabang.forms import *

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def fetch_rs():
    with connection.cursor() as get_all_rs:
        get_all_rs.execute('select * from medikago.rs_cabang')
        result = dictfetchall(get_all_rs)
        get_all_rs.close()
    return result


def rs_is_not_exist(kode_rs=None):
    if kode_rs == None:
        return

    with connection.cursor() as rs_exist:
        rs_exist.execute('select * from medikago.rs_cabang where kode_rs=%s', [kode_rs])
        rs = rs_exist.fetchone()
        rs_exist.close()
    return rs is None


# Create your views here.
def buat_rs(request):
    form = RSForm

    context = {
        'page_title': 'Buat RS',
        'form': form
    }

    if request.method == "POST":
        form = form(request.POST)
        if form.is_valid():
            if rs_is_not_exist(request.POST.get('kode_rs')):
                kode_rs = form.cleaned_data.get('kode_rs')
                nama = form.cleaned_data.get('nama')
                tanggal_pendirian = form.cleaned_data.get('tanggal_pendirian').strftime('%Y-%m-%d')
                jalan = form.cleaned_data.get('jalan')
                nomor = form.cleaned_data.get('nomor')
                kota = form.cleaned_data.get('kota')

                with connection.cursor() as buat_rs:
                    buat_rs.execute('insert into medikago.rs_cabang values (%s, %s, %s, %s, %s, %s)',
                                    [kode_rs, nama, tanggal_pendirian, jalan, nomor, kota]
                                    )
                    buat_rs.close()

                return redirect('rs_cabang:daftar_rs')
            else:
                context['error'] = 'Sudah ada Rumah Sakit dengan Kode RS: ' + request.POST.get('kode_rs')
                context['form'] = form
                return render(request, 'rs_cabang/buat_rs.html', context)

    return render(request, 'rs_cabang/buat_rs.html', context)


def daftar_rs(request):
    rs_tuple = fetch_rs()

    context = {
        'page_title': 'Daftar RS',
        'rs_dict': rs_tuple
    }

    return render(request, 'rs_cabang/daftar_rs.html', context)


def update_rs(request):
    if request.method == 'GET':
        return redirect('rs_cabang:daftar_rs')

    form = RSForm

    context = {
        'page_title': 'Update RS Cabang',
        'form': form
    }

    if request.method == 'POST':
        if request.POST.get('edit') == 'true':
            with connection.cursor() as get_rs:
                get_rs.execute('select * from medikago.rs_cabang where kode_rs=%s', [request.POST.get('kode_rs')])
                rumah_sakit_query = get_rs.fetchone()
                get_rs.close()

            rumah_sakit = {
                'kode_rs': rumah_sakit_query[0],
                'nama': rumah_sakit_query[1],
                'tanggal_pendirian': rumah_sakit_query[2],
                'jalan': rumah_sakit_query[3],
                'nomor': rumah_sakit_query[4],
                'kota': rumah_sakit_query[5]
            }

            form = form(rumah_sakit)
            context['form'] = form
            context['kode_rs'] = rumah_sakit_query[0]

            return render(request, 'rs_cabang/update_rs.html', context)
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(kode_rs=request.POST.get('kode_rs'))
                return redirect('rs_cabang:daftar_rs')
    return render(request, 'rs_cabang/update_rs.html', context)


def delete_rs(request):
    if request.method == 'POST':
        kode_rs = request.POST.get('kode_rs')
        if kode_rs:
            with connection.cursor() as delete_rs:
                delete_rs.execute('delete from medikago.rs_cabang where kode_rs=%s', [kode_rs])
                delete_rs.close()
    return redirect("rs_cabang:daftar_rs")

# BEGIN UNGU #

def daftarkan_dokter(request):
    if request.session.get('user').get('role') != 'Administrator':
        return redirect('home')

    context = {
        'page_title': 'Buat Sesi Konsultasi'
    }

    form = DaftarkanDokterForm
    context['form'] = form

    if request.method == 'POST':
        form = form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('rs_cabang:daftar_dokter')
        else:
            context['form'] = form
            return render(request, 'rs_cabang/daftarkan_dokter.html', context)
    
    return render(request, 'rs_cabang/daftarkan_dokter.html', context)

def get_daftar_dokter():
    with connection.cursor() as cursor:
        cursor.execute('select * from medikago.dokter_rs_cabang order by kode_rs')
        list_daftar_dokter = dictfetchall(cursor)
        cursor.close()
    return list_daftar_dokter

def daftar_dokter(request):
    context = {
        'page_title': 'Daftar Dokter RS Cabang'
    }

    user = request.session.get('user')
    role = user.get('role')

    context['daftar_dokter'] = get_daftar_dokter()

    return render(request, 'rs_cabang/daftar_dokter.html', context)

def update_dokter(request):
    if request.method == 'GET':
        return redirect('rs_cabang:daftar_dokter')
    
    context = {
        'page_title': 'Update Dokter RS Cabang'
    }

    form = DaftarkanDokterForm
    context['form'] = form

    if request.method == "POST":
        id_dokter_old = request.POST.get('id_dokter_old')
        kode_rs_old = request.POST.get('kode_rs_old')
        if request.POST.get('edit') == 'true':
            with connection.cursor() as cursor:
                cursor.execute('select * from medikago.dokter_rs_cabang where id_dokter=%s and kode_rs=%s', 
                                [id_dokter_old, kode_rs_old])
                dokter_rs_cabang_query = cursor.fetchone()
                cursor.close()

            dokter_rs_cabang = {
                'id_dokter': dokter_rs_cabang_query[0],
                'kode_rs': dokter_rs_cabang_query[1],
            }

            form = form(dokter_rs_cabang)
            context['form'] = form
            context['id_dokter'] = dokter_rs_cabang_query[0]
            context['kode_rs'] = dokter_rs_cabang_query[1]
            context['id_dokter_old'] = id_dokter_old
            context['kode_rs_old'] = kode_rs_old

            return render(request, 'rs_cabang/update_dokter.html', context)
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(id_dokter_old=id_dokter_old, kode_rs_old=kode_rs_old)
                return redirect('rs_cabang:update_dokter')
    return render(request, 'rs_cabang/update_dokter.html', context)

def delete_dokter(request):
    if request.method == 'POST':
        id_dokter = request.POST.get('id_dokter')
        kode_rs = request.POST.get('kode_rs')
        with connection.cursor() as cursor:
            cursor.execute('delete from medikago.dokter_rs_cabang where id_dokter=%s and kode_rs=%s',
                            [id_dokter, kode_rs])
            cursor.close()
    return redirect("rs_cabang:daftar_dokter")

# END UNGU #

def daftar_admin(request):

    form = DaftarkanAdminForm()

    form.fields['nomor_pegawai'].widget.choices = get_nomor_pegawai()
    form.fields['kode_rs'].widget.choices = get_kode_rs()

    context = {
        'page_title': 'Daftar Administrator',
        'form': form
    }

    if request.method == 'POST':
        form=DaftarkanAdminForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            nomor_pegawai = form.cleaned_data['nomor_pegawai']
            kode_rs = form.cleaned_data['kode_rs']
            form.edit()
            form.fields['nomor_pegawai'].widget.choices = get_nomor_pegawai()
            form.fields['kode_rs'].widget.choices = get_kode_rs()
            return render(request, 'rs_cabang/daftar_admin.html', context)
        else:
            context[ 'form' ] = form
            form.fields['nomor_pegawai'].widget.choices = get_nomor_pegawai()
            form.fields['kode_rs'].widget.choices = get_kode_rs()
            return render(request, 'rs_cabang/daftar_admin.html', context)
                    
    return render(request, 'rs_cabang/daftar_admin.html', context)
