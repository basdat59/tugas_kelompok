from django.urls import path
from rs_cabang.views import buat_rs, daftar_admin, daftar_dokter, daftar_rs, daftarkan_dokter, delete_rs, update_dokter, delete_dokter, update_rs

app_name = 'rs_cabang'

urlpatterns = [
    path('buat/', buat_rs, name='buat_rs'),
    path('daftar/', daftar_rs, name='daftar_rs'),
    path('update/', update_rs, name='update_rs'),
    path('delete/', delete_rs, name='delete_rs'),
    path('dokter/daftarkan/', daftarkan_dokter, name='daftarkan_dokter'),
    path('dokter/daftar/', daftar_dokter, name='daftar_dokter'),
    path('dokter/update/', update_dokter, name='update_dokter'),
    path('dokter/delete/', delete_dokter, name='delete_dokter'),
    path('admin/daftar/', daftar_admin, name='daftar_admin')
]
