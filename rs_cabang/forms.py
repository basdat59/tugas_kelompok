from django import forms
from django.db import connection


year_choice = ["{0}".format(year) for year in range(1995, 2031)]


class RSForm(forms.Form):
    attrs = {'class': 'form-control'}

    kode_rs = forms.CharField(label='Kode RS Cabang', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    nama = forms.CharField(label='Nama', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    tanggal_pendirian = forms.DateField(label='Tanggal Pendirian', required=True, widget=forms.SelectDateWidget(attrs=attrs, years=year_choice))
    jalan = forms.CharField(label='Jalan', required=False, widget=forms.TextInput(attrs=attrs))
    nomor = forms.IntegerField(label='Nomor', required=False, widget=forms.NumberInput(attrs=attrs))
    kota = forms.CharField(label='Kota', required=True, widget=forms.TextInput(attrs=attrs))

    def save(self, commit=True):
        kode_rs = self.cleaned_data.get('kode_rs')
        nama = self.cleaned_data.get('nama')
        tanggal_pendirian = self.cleaned_data.get('tanggal_pendirian')
        jalan = self.cleaned_data.get('jalan')
        nomor = self.cleaned_data.get('nomor')
        kota = self.cleaned_data.get('kota')

        # Save to the database postgresql
        with connection.cursor() as pengguna:
            pengguna.execute('insert into rs_cabang values (%s, %s, %s, %s, %s, %s)',
                             [kode_rs, nama, tanggal_pendirian, jalan, nomor, kota])
            pengguna.close()

    def edit(self, commit=True, kode_rs=None):
        if not kode_rs:
            return

        kode_rs = self.cleaned_data.get('kode_rs')
        nama = self.cleaned_data.get('nama')
        tanggal_pendirian = self.cleaned_data.get('tanggal_pendirian').strftime('%Y-%m-%d')
        jalan = self.cleaned_data.get('jalan')
        nomor = self.cleaned_data.get('nomor')
        kota = self.cleaned_data.get('kota')

        with connection.cursor() as save_rs:
            query = 'update medikago.rs_cabang '
            query += 'set nama=%s, tanggal_pendirian=%s, jalan=%s, nomor=%s, kota=%s '
            query += 'where kode_rs=%s'
            save_rs.execute(query, [nama, tanggal_pendirian, jalan, nomor, kota, kode_rs])
            save_rs.close()


# BEGIN UNGU #

def fetch_to_choices(cursor_fetch):
    choices = []
    choices.append(("-", "-"))
    for item in cursor_fetch:
        choice = (item[0], item[0])
        choices.append(choice)
    return choices

def get_kode_rs():
    with connection.cursor() as cursor:
        cursor.execute('select kode_rs from medikago.rs_cabang')
        query = cursor.fetchall()
        cursor.close()
    return fetch_to_choices(query)

def get_id_dokter():
    with connection.cursor() as cursor:
        cursor.execute('select id_dokter from medikago.dokter')
        query = cursor.fetchall()
        cursor.close()
    return fetch_to_choices(query)

class DaftarkanDokterForm(forms.Form):
    attrs = {'class': 'form-control'}

    kode_rs_choices = get_kode_rs()
    id_dokter_choices = get_id_dokter()

    kode_rs = forms.CharField(label='Kode RS', required=True, widget=forms.Select(choices=kode_rs_choices, attrs=attrs))
    id_dokter = forms.CharField(label='ID Dokter', required=True, widget=forms.Select(choices=id_dokter_choices, attrs=attrs))

    def save(self, commit=True):
        kode_rs = self.cleaned_data.get('kode_rs')
        id_dokter = self.cleaned_data.get('id_dokter')

        with connection.cursor() as cursor:
            cursor.execute('insert into medikago.dokter_rs_cabang values (%s, %s)',
                             [id_dokter, kode_rs])
            cursor.close()
    
    def edit(self, commit=True, id_dokter_old=None, kode_rs_old=None):
        if not (id_dokter_old or kode_rs_old):
            return
        
        kode_rs = self.cleaned_data.get('kode_rs')
        id_dokter = self.cleaned_data.get('id_dokter')

        with connection.cursor() as cursor:
            cursor.execute('update medikago.dokter_rs_cabang set id_dokter=%s, kode_rs=%s where id_dokter=%s and kode_rs=%s', 
                            [id_dokter, kode_rs, id_dokter_old, kode_rs_old])
            cursor.close()

# END UNGU #


KODE_RS_CHOICES = [
    ('RS1', 'RS1'),
    ('RS2', 'RS2'),
    ('RS3', 'RS3'),
    ('RS4', 'RS4'),
    ('RS5', 'RS5')
]

# ID_DOKTER_CHOICES = [
#     ('DOK1', 'DOK1'),
#     ('DOK2', 'DOK2'),
#     ('DOK3', 'DOK3'),
#     ('DOK4', 'DOK4'),
#     ('DOK5', 'DOK5'),
#     ('DOK6', 'DOK6'),
#     ('DOK7', 'DOK7'),
#     ('DOK8', 'DOK8'),
#     ('DOK9', 'DOK9')
# ]

NOMOR_PEGAWAI_CHOICES = [
    ('8ac3c689-74a0-4953-85c6-f45aacead6e2', 'f45aacead6e2'),
    ('e16a9d1a-2141-4470-bb8a-855fbabd36fc', '855fbabd36fc'),
    ('71521487-1c57-4c7e-ac31-d08cc83005b9', 'd08cc83005b9')
]

def get_nomor_pegawai():
    with connection.cursor() as cursor:
        query = "select nomor_pegawai from medikago.administrator"
        cursor.execute(query)
        rows = cursor.fetchall()
        cursor.close()
    
    #print(rows)
    if rows is None:
        print("None")
    return [tuple((item[0], item[0]))
    for item in rows]

def get_kode_rs():
    with connection.cursor() as cursor:
        query = "select kode_rs from medikago.rs_cabang"
        cursor.execute(query)
        rows = cursor.fetchall()
        cursor.close()

    #print(rows)
    if rows is None:
        print("None")
    return [tuple((item[0], item[0]))
    for item in rows]
  

class DaftarkanAdminForm(forms.Form):
    attrs = {'class': 'form-control'}

    daftar_pegawai = get_nomor_pegawai()
    daftar_rs_cabang = get_kode_rs()

    #def __init__(self, data=None, *args, **kwargs):
    #    super(DaftarkanAdminForm, self).__init__(*args, **kwargs)
    #    if data:
    #        self.fields['nomor_pegawai'] = forms.ChoiceField(label="Nomor Pegawai", required=True, choices=data[0])
    #        self.fields['kode_rs'] = forms.ChoiceField(label="Kode RS", required=True, choices=data[1])

    nomor_pegawai = forms.ChoiceField(label='Nomor Pegawai', required=True, choices=daftar_pegawai)
    kode_rs = forms.ChoiceField(label="Kode RS", required=True, choices=daftar_rs_cabang)

    #print(daftar_pegawai)
    #print(daftar_rs_cabang)

    def edit(self, commit=True):
        nomor_pegawai = self.cleaned_data.get('nomor_pegawai')
        kode_rs = self.cleaned_data.get('kode_rs')

        print(kode_rs)

        with connection.cursor() as edit_admin:
            query = "update medikago.administrator "
            query += "set kode_rs = %s where nomor_pegawai = %s"
            edit_admin.execute(query, [kode_rs, nomor_pegawai])
            edit_admin.close()