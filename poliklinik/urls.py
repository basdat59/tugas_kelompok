from django.urls import path
from poliklinik.views import *

app_name = 'poliklinik'

urlpatterns = [
    path('layanan/buat/', buat_layanan_poliklinik, name='buat_layanan'),
    path('layanan/daftar', daftar_layanan_poliklinik, name='daftar_layanan'),
    path('layanan/update/', update_layanan_poliklinik, name='update_layanan'),
    path('layanan/delete/', delete_layanan_poliklinik, name='delete_layanan'),
    path('jadwal/daftar', daftar_jadwal_poliklinik, name='daftar_jadwal'),
    path('jadwal/update/', update_jadwal_poliklinik, name='update_jadwal'),
    path('jadwal/delete/', delete_jadwal_poliklinik, name='delete_jadwal')
]
