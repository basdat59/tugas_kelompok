from django.shortcuts import render, redirect
from .forms import *

# Create your views here.

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def fetch_to_choices(cursor_fetch):
    choices = []
    choices.append(("-", "-"))
    for item in cursor_fetch:
        choice = (item[0], item[0])
        choices.append(choice)
    return choices

def id_poliklinik_generator():
    with connection.cursor() as cursor:
        query = "select id_poliklinik from "
        query += "medikago.layanan_poliklinik order by "
        query += "nullif(regexp_replace(id_poliklinik, '\D', '', 'g'), '')::int desc limit 1"
        cursor.execute(query)
        row = cursor.fetchone()
        cursor.close()
    
    last_nomor = int(row[0][2:]) if (row is not None) else None
   
    if last_nomor is None:
        return 'LP' + str(1)
    
    return 'LP' + str(last_nomor + 1)

def id_jadwal_poliklinik_generator():
    with connection.cursor() as cursor:
        query = "select id_jadwal_poliklinik from "
        query += "medikago.jadwal_layanan_poliklinik order by "
        query += "nullif(regexp_replace(id_jadwal_poliklinik, '\D', '', 'g'), '')::int desc limit 1"
        cursor.execute(query)
        row = cursor.fetchone()
        cursor.close()
    
    last_nomor = int(row[0][3:]) if (row is not None) else None
   
    if last_nomor is None:
        return 'JLP' + str(1)
    
    return 'JLP' + str(last_nomor + 1)

def get_daftar_layanan():
    with connection.cursor() as cursor:
        cursor.execute('select * from medikago.layanan_poliklinik order by id_poliklinik')
        list_layanan = dictfetchall(cursor)
        cursor.close()
    return list_layanan

# BUAT LAYANAN
def buat_layanan_poliklinik(request):
    form = LayananPoliklinikForm
    context = {
        'page_title': 'Buat Layanan Poliklinik',
        'form': form
    }

    if request.method == "POST":
        form = form(request.POST)
        if form.is_valid():
            id_poliklinik = id_poliklinik_generator()
            kode_rs_cabang = form.cleaned_data.get('kode_rs_cabang')
            nama = form.cleaned_data.get('nama')
            deskripsi = form.cleaned_data.get('deskripsi')

            with connection.cursor() as cursor:
                cursor.execute('insert into medikago.layanan_poliklinik values (%s, %s, %s, %s)',
                                [id_poliklinik, kode_rs_cabang, nama, deskripsi])
                cursor.close()

            # Jadwal Poliklinik
            hari = request.POST.getlist('hari[]')
            jam_mulai = request.POST.getlist('jam_mulai[]')
            jam_selesai = request.POST.getlist('jam_selesai[]')
            kapasitas = request.POST.getlist('kapasitas[]')

            for i in range(len(hari)):
                with connection.cursor() as cursor:
                    query = "select id_dokter from medikago.dokter_rs_cabang "
                    query += "where kode_rs=%s "
                    query += "order by random() limit 1"
                    cursor.execute(query, [kode_rs_cabang])
                    id_dokter = cursor.fetchone()
                    cursor.close()
                
                if id_dokter is None:
                    context['error'] = "Tidak ada dokter pada RS Cabang yang dipilih."
                else:
                    id_dokter = id_dokter[0]
                    id_jadwal_poliklinik = id_jadwal_poliklinik_generator()
                    hari_i = hari[i]
                    jam_mulai_i = jam_mulai[i]
                    jam_selesai_i = jam_selesai[i]
                    kapasitas_i = kapasitas[i]
                
                    with connection.cursor() as cursor:
                        cursor.execute("insert into medikago.jadwal_layanan_poliklinik values (%s, %s, %s, %s, %s, %s, %s)",
                                        [id_jadwal_poliklinik, jam_mulai_i, jam_selesai_i, hari_i, kapasitas_i, id_dokter, id_poliklinik])
                        cursor.close()

    return render(request, 'poliklinik/buat_layanan.html', context)

# DAFTAR LAYANAN
def daftar_layanan_poliklinik(request):
    context = {
        'page_title': 'Daftar Layanan Poliklinik'
    }

    user = request.session.get('user')
    role = user.get('role')

    context['daftar_layanan'] = get_daftar_layanan()

    return render(request, 'poliklinik/daftar_layanan.html', context)

# DAFTAR JADWAL
def daftar_jadwal_poliklinik(request):
    context = {
        'page_title': 'Daftar Jadwal Layanan Poliklinik'
    }
    if request.method == "GET":
        with connection.cursor() as cursor:
            cursor.execute('select * from medikago.jadwal_layanan_poliklinik order by id_jadwal_poliklinik')
            list_daftar_jadwal = dictfetchall(cursor)
            cursor.close()
        
        context['daftar_jadwal'] = list_daftar_jadwal
    else:        
        id_poliklinik = request.POST['id_poliklinik']

        with connection.cursor() as cursor:
            cursor.execute('select * from medikago.jadwal_layanan_poliklinik where id_poliklinik=%s order by id_jadwal_poliklinik', [id_poliklinik])
            list_daftar_jadwal = dictfetchall(cursor)
            cursor.close()
        
        context['daftar_jadwal'] = list_daftar_jadwal
    return render(request, 'poliklinik/daftar_jadwal.html', context)

# UPDATE LAYANAN
def update_layanan_poliklinik(request):
    form = UpdateLayananForm
    context = {
        'page_title': 'Buat Layanan Poliklinik',
        'form': form
    }
    if request.method == "POST":
        if request.POST.get('edit') == 'true':
            with connection.cursor() as cursor:
                cursor.execute('select * from medikago.layanan_poliklinik where id_poliklinik=%s', 
                                [request.POST.get('id_poliklinik')])
                layanan_poliklinik_query = cursor.fetchone()
                cursor.close()

            layanan_poliklinik = {
                'id_poliklinik': layanan_poliklinik_query[0],
                'kode_rs_cabang': layanan_poliklinik_query[1],
                'nama': layanan_poliklinik_query[2],
                'deskripsi': layanan_poliklinik_query[3]
            }

            form = form(layanan_poliklinik)
            context['form'] = form
            context['id_poliklinik'] = layanan_poliklinik_query[0]

            return render(request, 'poliklinik/update_layanan.html', context)
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(id_poliklinik=request.POST.get('id_poliklinik'))
                with connection.cursor() as cursor:
                    cursor.execute('select * from medikago.jadwal_layanan_poliklinik where id_poliklinik=%s', 
                                    [request.POST.get('id_poliklinik')])
                    jadwal_poliklinik_query = cursor.fetchall()
                    cursor.close()
                
                for i in range(len(jadwal_poliklinik_query)):
                    id_jadwal_poliklinik = jadwal_poliklinik_query[i][0]
                    id_poliklinik = jadwal_poliklinik_query[i][6]

                    with connection.cursor() as cursor:
                        query = 'select id_dokter from medikago.dokter_rs_cabang where kode_rs in '
                        query += '(select kode_rs_cabang from medikago.layanan_poliklinik where id_poliklinik=%s) '
                        query += 'order by random() limit 1'
                        cursor.execute(query, [id_poliklinik])
                        id_dokter = cursor.fetchall()
                        cursor.close()
                    
                    id_dokter = id_dokter[0][0]

                    with connection.cursor() as cursor:
                        cursor.execute('update medikago.jadwal_layanan_poliklinik set id_dokter=%s where id_jadwal_poliklinik=%s',
                                        [id_dokter, id_jadwal_poliklinik])
                        cursor.close()
                return redirect('poliklinik:daftar_layanan')
    return render(request, 'poliklinik/update_layanan.html', context)    

# UPDATE JADWAL
def update_jadwal_poliklinik(request):
    form = UpdateJadwalForm
    context = {
        'page_title': 'Buat Layanan Poliklinik',
    }

    if request.method == "POST":
        if request.POST.get('edit') == 'true':
            with connection.cursor() as cursor:
                cursor.execute('select * from medikago.jadwal_layanan_poliklinik where id_jadwal_poliklinik=%s', 
                                [request.POST.get('id_jadwal_poliklinik')])
                jadwal_poliklinik_query = cursor.fetchone()
                cursor.close()

            jadwal_poliklinik = {
                'id_jadwal_poliklinik' : jadwal_poliklinik_query[0],
                'waktu_mulai': jadwal_poliklinik_query[1],
                'waktu_selesai': jadwal_poliklinik_query[2],
                'hari': jadwal_poliklinik_query[3],
                'kapasitas': jadwal_poliklinik_query[4],
                'id_dokter' : jadwal_poliklinik_query[5],
                'id_poliklinik' : jadwal_poliklinik_query[6]
            }            

            form = form(request.POST.get('id_poliklinik'), jadwal_poliklinik)

            context['form'] = form
            context['id_jadwal_poliklinik'] = jadwal_poliklinik_query[0]
            context['id_poliklinik'] = jadwal_poliklinik_query[6]

            return render(request, 'poliklinik/update_jadwal.html', context)
        else:
            form = form(request.POST.get('id_poliklinik'), request.POST)
            if form.is_valid():
                form.edit(id_jadwal_poliklinik=request.POST.get('id_jadwal_poliklinik'))
                context_else = {
                    'page_title': 'Daftar Jadwal Layanan Poliklinik'
                }
                
                id_poliklinik = request.POST.get('id_poliklinik')

                with connection.cursor() as cursor:
                    cursor.execute('select * from medikago.jadwal_layanan_poliklinik where id_poliklinik=%s order by id_jadwal_poliklinik', [id_poliklinik])
                    list_daftar_jadwal = dictfetchall(cursor)
                    cursor.close()
                
                context_else['daftar_jadwal'] = list_daftar_jadwal
            return render(request, 'poliklinik/daftar_jadwal.html', context_else)
    return render(request, 'poliklinik/update_jadwal.html', context)

# DELETE LAYANAN
def delete_layanan_poliklinik(request):
    if request.method == 'POST':
        id_poliklinik = request.POST.get('id_poliklinik')
        if id_poliklinik:            
            with connection.cursor() as cursor:
                cursor.execute('select * from medikago.jadwal_layanan_poliklinik where id_poliklinik=%s', [id_poliklinik])
                list_daftar_jadwal = cursor.fetchall()
                cursor.close()

            for i in range(len(list_daftar_jadwal)):
                id_jadwal_poliklinik = list_daftar_jadwal[i][0]

                with connection.cursor() as cursor:
                    cursor.execute('delete from medikago.jadwal_layanan_poliklinik where id_jadwal_poliklinik=%s', [id_jadwal_poliklinik])
                    cursor.close()
            
            with connection.cursor() as cursor:
                cursor.execute('delete from medikago.layanan_poliklinik where id_poliklinik=%s', [id_poliklinik])
                cursor.close()
    return redirect("poliklinik:daftar_layanan")

# DELETE JADWAL
def delete_jadwal_poliklinik(request):
    if request.method == 'POST':
        id_jadwal_poliklinik = request.POST.get('id_jadwal_poliklinik')
        id_poliklinik = request.POST.get('id_poliklinik')
        if id_jadwal_poliklinik:
            with connection.cursor() as cursor:
                cursor.execute('delete from medikago.jadwal_layanan_poliklinik where id_jadwal_poliklinik=%s', [id_jadwal_poliklinik])
                cursor.close()
        
        context = {
            'page_title': 'Daftar Jadwal Layanan Poliklinik'
        }

        with connection.cursor() as cursor:
            cursor.execute('select * from medikago.jadwal_layanan_poliklinik where id_poliklinik=%s order by id_jadwal_poliklinik', [id_poliklinik])
            list_daftar_jadwal = dictfetchall(cursor)
            cursor.close()
        
        context['daftar_jadwal'] = list_daftar_jadwal
    return render(request, 'poliklinik/daftar_jadwal.html', context)
