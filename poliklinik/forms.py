from django import forms
from django.forms import formset_factory
from django.db import connection

def fetch_to_choices(cursor_fetch):
    choices = []
    choices.append(("-", "-"))
    for item in cursor_fetch:
        choice = (item[0], item[0])
        choices.append(choice)
    return choices

def get_kode_rs():
    with connection.cursor() as cursor:
        cursor.execute('select kode_rs from medikago.rs_cabang')
        query = cursor.fetchall()
        cursor.close()
    return fetch_to_choices(query)

class LayananPoliklinikForm(forms.Form):
    attrs = {'class': 'form-control'}
    kode_rs_choices = get_kode_rs()
    
    nama = forms.CharField(label='Nama Layanan', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    deskripsi = forms.CharField(label='Deskripsi', required=False, widget=forms.Textarea(attrs=attrs))
    kode_rs_cabang = forms.CharField(label='Kode RS', required=True, widget=forms.Select(choices=kode_rs_choices, attrs=attrs))

class UpdateLayananForm(forms.Form):
    attrs = {'class': 'form-control'}
    kode_rs_choices = get_kode_rs()
    
    id_poliklinik = forms.CharField(label='ID Poliklinik', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    nama = forms.CharField(label='Nama Layanan', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    deskripsi = forms.CharField(label='Deskripsi', required=False, widget=forms.Textarea(attrs=attrs))
    kode_rs_cabang = forms.CharField(label='Kode RS', required=True, widget=forms.Select(choices=kode_rs_choices, attrs=attrs))

    def edit(self, commit=True, id_poliklinik=None):
        if not id_poliklinik:
            return
        
        id_poliklinik = self.cleaned_data.get('id_poliklinik')
        nama = self.cleaned_data.get('nama')
        deskripsi = self.cleaned_data.get('deskripsi')
        kode_rs_cabang = self.cleaned_data.get('kode_rs_cabang')

        with connection.cursor() as cursor:
            cursor.execute('update medikago.layanan_poliklinik set kode_rs_cabang=%s, nama=%s, deskripsi=%s where id_poliklinik=%s',
                            [kode_rs_cabang, nama, deskripsi, id_poliklinik])
            cursor.close()

class UpdateJadwalForm(forms.Form):
    attrs = {'class': 'form-control'}

    id_jadwal_poliklinik = forms.CharField(label='ID Jadwal Poliklinik', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    hari = forms.CharField(label='Hari', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    waktu_mulai = forms.TimeField(label='Waktu Mulai', required=True, widget=forms.TimeInput(attrs=attrs))
    waktu_selesai = forms.TimeField(label='Waktu Selesai', required=True, widget=forms.TimeInput(attrs=attrs))
    kapasitas = forms.IntegerField(label='Kapasitas', required=True, widget=forms.NumberInput(attrs=attrs))
    id_dokter = forms.CharField(label='ID Dokter', required=True, max_length=50)
    id_poliklinik = forms.CharField(label='ID Poliklinik', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))

    def __init__(self, id_lp, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        with connection.cursor() as cursor:
            query = 'select id_dokter from medikago.dokter_rs_cabang where kode_rs in '
            query += '(select kode_rs_cabang from medikago.layanan_poliklinik where id_poliklinik=%s)'
            cursor.execute(query, [id_lp])
            query = cursor.fetchall()
            cursor.close()
        id_dokter_choices = fetch_to_choices(query)

        attrs = {'class': 'form-control'}
        self.fields['id_dokter'].widget = forms.Select(choices=id_dokter_choices, attrs=attrs)
    
    def edit(self, commit=True, id_jadwal_poliklinik=None):
        if not id_jadwal_poliklinik:
            return
        
        id_jadwal_poliklinik = self.cleaned_data.get('id_jadwal_poliklinik')
        hari = self.cleaned_data.get('hari')
        waktu_mulai = self.cleaned_data.get('waktu_mulai')
        waktu_selesai = self.cleaned_data.get('waktu_selesai')
        kapasitas = self.cleaned_data.get('kapasitas')
        id_dokter = self.cleaned_data.get('id_dokter')
        id_poliklinik = self.cleaned_data.get('id_poliklinik')

        with connection.cursor() as cursor:
            cursor.execute('update medikago.jadwal_layanan_poliklinik set hari=%s, waktu_mulai=%s, waktu_selesai=%s, kapasitas=%s, id_dokter=%s where id_jadwal_poliklinik=%s',
                            [hari, waktu_mulai, waktu_selesai, kapasitas, id_dokter, id_jadwal_poliklinik])
            cursor.close()