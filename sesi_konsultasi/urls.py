from django.urls import path
from sesi_konsultasi.views import buat_sesi, daftar_sesi, delete_sesi, get_id_transaksi, update_sesi

app_name = 'sesi_konsultasi'

urlpatterns = [
    path('buat/', buat_sesi, name='buat_sesi'),
    path('daftar/', daftar_sesi, name='daftar_sesi'),
    path('update/', update_sesi, name='update_sesi'),
    path('get_id_transaksi/', get_id_transaksi, name='get_id_transaksi'),
    path('delete/', delete_sesi, name='delete_sesi')
]
