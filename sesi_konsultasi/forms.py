from django import forms
from django.db import connection


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def id_konsultasi_generator():
    with connection.cursor() as cursor:
        query = "select id_konsultasi from "
        query += "medikago.sesi_konsultasi order by "
        query += "nullif(regexp_replace(id_konsultasi, '\D', '', 'g'), '')::int desc limit 1"
        cursor.execute(query)
        row = cursor.fetchone()
        cursor.close()
    
    last_id = int(row[0][2:]) if (row is not None) else None

    if last_id is None:
        return 'SK' + str(1)
    
    return 'SK' + str(last_id + 1)

def get_nomor_rekam_medis():
    with connection.cursor() as cursor:
        cursor.execute('select no_rekam_medis from medikago.pasien')
        query = cursor.fetchall()
        cursor.close()
    return fetch_to_choices(query)


def get_id_transaksi():
    with connection.cursor() as cursor:
        cursor.execute('select id_transaksi from medikago.transaksi')
        query = cursor.fetchall()
        cursor.close()
    return fetch_to_choices(query)


def fetch_to_choices(cursor_fetch):
    choices = []
    choices.append(("-", "-"))
    for no_rekam_medis in cursor_fetch:
        choice = (no_rekam_medis[0], no_rekam_medis[0])
        choices.append(choice)
    return choices


class SesiKonsultasiForm(forms.Form):
    attrs = {'class': 'form-control'}  
    no_rekam_choices = get_nomor_rekam_medis()
    id_transaksi_choices = get_id_transaksi()

    no_rekam_medis_pasien = forms.ChoiceField(label="No Rekam Medis Pasien",
                                                required=True,
                                                choices=no_rekam_choices,
                                                widget=forms.Select(attrs=attrs))
    tanggal = forms.DateField(label='Tanggal', required=True, widget=forms.SelectDateWidget(attrs=attrs))
    id_transaksi = forms.ChoiceField(label="ID Transaksi",
                                    required=True, 
                                    choices=id_transaksi_choices,
                                    widget=forms.Select(attrs=attrs))

    def save(self, commit=True):
        id_konsultasi = id_konsultasi_generator()
        no_rekam_medis = self.cleaned_data.get('no_rekam_medis_pasien')
        tanggal = self.cleaned_data.get('tanggal').strftime('%Y-%m-%d')
        biaya = 0
        status = "Created"
        id_transaksi = self.cleaned_data.get('id_transaksi')

        with connection.cursor() as cursor:
            cursor.execute('insert into medikago.sesi_konsultasi values (%s, %s, %s, %s, %s, %s)',
                            [id_konsultasi, no_rekam_medis, tanggal, biaya, status, id_transaksi])
            cursor.close()

class SesiKonsultasiUpdateForm(forms.Form):
    attrs = {'class': 'form-control'}  
    id_konsultasi = forms.CharField(label="ID Konsultasi", required=True, widget=forms.TextInput(attrs=attrs))
    no_rekam_medis = forms.CharField(label="Nomor Rekam Medis Pasien", required=True, widget=forms.TextInput(attrs=attrs))
    tanggal = forms.DateField(label='Tanggal', required=True, widget=forms.SelectDateWidget(attrs=attrs))
    biaya = forms.CharField(label="Biaya", required=True, widget=forms.TextInput(attrs=attrs))
    status = forms.CharField(label="Status", required=True, widget=forms.TextInput(attrs=attrs))
    id_transaksi = forms.CharField(label="ID Transaksi", required=True, widget=forms.TextInput(attrs=attrs))

    def edit(self, commit=True, id_konsultasi=None):
        if not id_konsultasi:
            return
        
        id_konsultasi = self.cleaned_data.get('id_konsultasi')
        no_rekam_medis_pasien = self.cleaned_data.get('no_rekam_medis')
        tanggal = self.cleaned_data.get('tanggal').strftime('%Y-%m-%d')
        biaya = self.cleaned_data.get('biaya')
        status = self.cleaned_data.get('status')
        id_transaksi = self.cleaned_data.get('id_transaksi')

        with connection.cursor() as cursor:
            query = 'update medikago.sesi_konsultasi '
            query += 'set tanggal=%s, status=%s where id_konsultasi=%s'
            cursor.execute(query, [tanggal, status, id_konsultasi])
            cursor.close()
