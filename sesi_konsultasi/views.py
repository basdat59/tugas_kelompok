from django.shortcuts import redirect, render
from django.db import connection
from django.http import JsonResponse
from sesi_konsultasi.forms import SesiKonsultasiForm, SesiKonsultasiUpdateForm


# Create your views here.
def buat_sesi(request):
    # ADMIN ONLY
    if request.session.get('user').get('role') != 'Administrator':
        return redirect('home')

    context = {
        'page_title': 'Buat Sesi Konsultasi'
    }
    
    #Ambil ID Transaksi Pasien secara Async?

    form = SesiKonsultasiForm
    context['form'] = form

    if request.method == 'POST':
        # Id_konsultasi otomatis oleh sistem, +1 ID konsul yang lama
        # Biaya Default = 0
        # status otomatis 'booked'
        form = form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('sesi_konsultasi:daftar_sesi')
        else:
            context['form'] = form
            return render(request, 'sesi_konsultasi/buat_sesi.html', context)
    
    return render(request, 'sesi_konsultasi/buat_sesi.html', context)
    

def daftar_sesi(request):
    context = {
        'page_title': 'Daftar Sesi Konsultasi'
    }

    user = request.session.get('user')
    role = user.get('role')

    if role == "Administrator":
        daftar_sesi_konsultasi = get_daftar_sesi_konsultasi_admin()
    elif role == "Pasien":
        daftar_sesi_konsultasi = get_daftar_sesi_konsultasi_pasien(user.get('no_rekam_medis'))
    elif role == "Dokter":
        daftar_sesi_konsultasi = get_daftar_sesi_konsultasi_dokter(user.get('id_dokter'))

    context['daftar_sk'] = daftar_sesi_konsultasi

    return render(request, 'sesi_konsultasi/daftar_sesi.html', context)


def update_sesi(request):
    if request.method == 'GET':
        return redirect('sesi_konsultasi:daftar_sesi')
    
    context = {
        'page_title': 'Update Sesi Konsultasi'
    }

    form = SesiKonsultasiUpdateForm
    context['form'] = form

    if request.method == "POST":
        if request.POST.get('edit') == 'true':
            with connection.cursor() as cursor:
                cursor.execute('select * from medikago.sesi_konsultasi where id_konsultasi=%s', 
                                [request.POST.get('id_konsultasi')])
                sesi_konsultasi_query = cursor.fetchone()
                cursor.close()

            sesi_konsultasi = {
                'id_konsultasi': sesi_konsultasi_query[0],
                'no_rekam_medis': sesi_konsultasi_query[1],
                'tanggal': sesi_konsultasi_query[2],
                'biaya': sesi_konsultasi_query[3],
                'status': sesi_konsultasi_query[4],
                'id_transaksi': sesi_konsultasi_query[5]
            }

            form = form(sesi_konsultasi)
            context['form'] = form
            context['id_konsultasi'] = sesi_konsultasi_query[0]
            context['no_rekam_medis'] = sesi_konsultasi_query[1]
            context['biaya'] = sesi_konsultasi_query[3]
            context['id_transaksi'] = sesi_konsultasi_query[5]

            return render(request, 'sesi_konsultasi/update_sesi.html', context)
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(id_konsultasi=request.POST.get('id_konsultasi'))
                return redirect('sesi_konsultasi:daftar_sesi')
    return render(request, 'sesi_konsultasi/update_sesi.html', context)


def delete_sesi(request):
    if request.method == 'POST':
        id_konsultasi = request.POST.get('id_konsultasi')
        if id_konsultasi:
            with connection.cursor() as cursor:
                cursor.execute('delete from medikago.sesi_konsultasi where id_konsultasi=%s', [id_konsultasi])
                cursor.close()
    return redirect("sesi_konsultasi:daftar_sesi")


def id_transaksi_fetch_to_list(cursor_fetch):
    id_transaksi_list = []
    for row in cursor_fetch:
        id_transaksi = row[0]
        id_transaksi_list.append(id_transaksi)
    return id_transaksi_list


def get_id_transaksi(request):
    no_rekam_medis = request.GET.get('no_rekam_medis', None)

    with connection.cursor() as cursor:
        cursor.execute('select id_transaksi from medikago.transaksi where no_rekam_medis=%s', [no_rekam_medis])
        query = cursor.fetchall()
        cursor.close()

    data = {}
    if (len(query) == 0):
        data['error'] = no_rekam_medis + " tidak memiliki transaksi."
    else:
        data['transaksi'] = id_transaksi_fetch_to_list(query)

    return JsonResponse(data)


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def get_daftar_sesi_konsultasi_admin():
    with connection.cursor() as cursor:
        cursor.execute('select * from medikago.sesi_konsultasi')
        list_sesi_konsultasi_admin = dictfetchall(cursor)
        cursor.close()
    return list_sesi_konsultasi_admin


def get_daftar_sesi_konsultasi_pasien(no_rekam_medis):
    with connection.cursor() as cursor:
        cursor.execute('select * from medikago.sesi_konsultasi where no_rekam_medis_pasien=%s',
                        [no_rekam_medis])
        list_sesi_konsultasi_pasien = dictfetchall(cursor)
        cursor.close()
    return list_sesi_konsultasi_pasien


def get_daftar_sesi_konsultasi_dokter(id_dokter):
    with connection.cursor() as cursor:
        query = 'select distinct sk.* from medikago.dokter d '
        query += 'join medikago.jadwal_layanan_poliklinik jlp on jlp.id_dokter = d.id_dokter '
        query += 'join medikago.layanan_poliklinik lp on lp.id_poliklinik = jlp.id_poliklinik '
        query += 'join medikago.tindakan_poli tp on tp.id_poliklinik = lp.id_poliklinik '
        query += 'join medikago.daftar_tindakan dt on dt.id_tindakan_poli = tp.id_tindakan_poli '
        query += 'join medikago.tindakan t1 on t1.id_konsultasi = dt.id_konsultasi '
        query += 'join medikago.tindakan t2 on t2.no_urut = dt.no_urut '
        query += 'join medikago.sesi_konsultasi sk on sk.id_konsultasi = t2.id_konsultasi '
        query += 'where d.id_dokter=%s'
        cursor.execute(query, [id_dokter])
        list_sesi_konsultasi_dokter = dictfetchall(cursor)
        cursor.close()
    return list_sesi_konsultasi_dokter
        