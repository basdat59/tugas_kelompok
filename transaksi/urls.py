from django.urls import path
from transaksi.views import daftar_transaksi, buat_transaksi, update_transaksi, delete_transaksi

app_name = 'transaksi'

urlpatterns = [
    path('buat/', buat_transaksi, name='buat_transaksi'),
    path('daftar/', daftar_transaksi, name='daftar_transaksi'),
    path('update/', update_transaksi, name='update_transaksi'),
    path('delete/', delete_transaksi, name='delete_transaksi')
]
