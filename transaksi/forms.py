from django import forms
from django.db import connection
import datetime

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def id_transaksi_generator():
    with connection.cursor() as cursor:
        query = "select id_transaksi from "
        query += "medikago.transaksi order by "
        query += "nullif(regexp_replace(id_transaksi, '\D', '', 'g'), '')::int desc limit 1"
        cursor.execute(query)
        row = cursor.fetchone()
        cursor.close()
    
    last_id = int(row[0][2:]) if (row is not None) else None

    if last_id is None:
        return 'TR' + str(1)
    
    return 'TR' + str(last_id + 1)

def get_nomor_rekam_medis():
    with connection.cursor() as cursor:
        cursor.execute('select no_rekam_medis from medikago.pasien')
        query = cursor.fetchall()
        cursor.close()
    return fetch_to_choices(query)

def fetch_to_choices(cursor_fetch):
    choices = []
    choices.append(("-", "-"))
    for no_rekam_medis in cursor_fetch:
        choice = (no_rekam_medis[0], no_rekam_medis[0])
        choices.append(choice)
    return choices

class TransaksiForm(forms.Form):
    attrs = {'class': 'form-control'}  
    no_rekam_choices = get_nomor_rekam_medis()

    no_rekam_medis_pasien = forms.ChoiceField(label="No Rekam Medis Pasien",
                                                required=True,
                                                choices=no_rekam_choices,
                                                widget=forms.Select(attrs=attrs))
    now = datetime.datetime.now()

    def save(self, commit=True):
        id_transaksi = id_transaksi_generator()
        tanggal = self.cleaned_data.get('now')
        status = "Created"
        biaya = 0
        bayar = self.cleaned_data.get('now')
        no_rekam_medis = self.cleaned_data.get('no_rekam_medis_pasien')

        with connection.cursor() as cursor:
            cursor.execute("set timezone to 'Asia/Jakarta'")
            cursor.execute('insert into medikago.transaksi values (%s, now(), %s, %s, now(), %s)',
                            [id_transaksi, status, biaya, no_rekam_medis])
            cursor.execute("set timezone to 'UTC'")
            cursor.close()
            
class UpdateTransaksiForm(forms.Form):
    attrs = {'class': 'form-control'}

    id_transaksi = forms.CharField(label="ID Transaksi", required=True, widget=forms.TextInput(attrs=attrs))
    tanggal = forms.DateField(label='Tanggal', required=True, widget=forms.SelectDateWidget(attrs=attrs))
    status = forms.CharField(label='Status', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    total_biaya = forms.CharField(label="Total Biaya", required=True, widget=forms.TextInput(attrs=attrs))
    waktu_pembayaran = forms.DateTimeField(label='Waktu Pembayaran', required=True, widget=forms.DateTimeInput(attrs=attrs))
    no_rekam_medis = forms.CharField(label="Nomor Rekam Medis Pasien", required=True, widget=forms.TextInput(attrs=attrs))   

    def edit(self, commit=True, id_transaksi=None):
        if not id_transaksi:
            return
        
        id_transaksi = self.cleaned_data.get('id_transaksi')
        tanggal = self.cleaned_data.get('tanggal').strftime('%Y-%m-%d')
        status = self.cleaned_data.get('status')
        total_biaya = self.cleaned_data.get('total_biaya')
        waktu_pembayaran = self.cleaned_data.get('waktu_pembayaran').strftime('%Y-%m-%d %H:%M:%S')
        no_rekam_medis_pasien = self.cleaned_data.get('no_rekam_medis')

        with connection.cursor() as cursor:
            query = 'update medikago.transaksi '
            query += 'set tanggal=%s, status=%s, waktu_pembayaran=%s where id_transaksi=%s'
            cursor.execute(query, [tanggal, status, waktu_pembayaran, id_transaksi])
            cursor.close()

no_rekam_medis_choices = [
    ('7946435177', '7946435177'),
    ('9291127655', '9291127655'),
    ('2296767427', '2296767427'),
    ('1017607303', '1017607303'),
    ('7835205858', '7835205858'),
    ('875430996', '875430996'),
    ('8198222170', '8198222170'),
    ('8198222170', '8198222170')
]