from django.shortcuts import redirect, render
from django.db import connection
from django.http import JsonResponse
from transaksi.forms import *

# Create your views here.
def daftar_transaksi(request):
    context = {
        'page_title': 'Daftar Transaksi'
    }

    context['daftar_t'] = get_daftar_transaksi()

    return render(request, 'transaksi/daftar.html', context)

def update_transaksi(request):
    if request.method == 'GET':
        return redirect('transaksi:daftar_transaksi')
    
    context = {
        'page_title': 'Update Transaksi'
    }

    form = UpdateTransaksiForm
    context['form'] = form

    if request.method == "POST":
        if request.POST.get('edit') == 'true':
            with connection.cursor() as cursor:
                cursor.execute('select * from medikago.transaksi where id_transaksi=%s', 
                                [request.POST.get('id_transaksi')])
                transaksi_query = cursor.fetchone()
                cursor.close()

            transaksi = {
                'id_transaksi': transaksi_query[0],
                'tanggal': transaksi_query[1],
                'status': transaksi_query[2],
                'total_biaya': transaksi_query[3],
                'waktu_pembayaran': transaksi_query[4],
                'no_rekam_medis': transaksi_query[5]
            }

            form = form(transaksi)
            context['form'] = form
            context['id_transaksi'] = transaksi_query[0]
            context['total_biaya'] = transaksi_query[3]
            context['no_rekam_medis'] = transaksi_query[5]

            return render(request, 'transaksi/update_transaksi.html', context)
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(id_transaksi=request.POST.get('id_transaksi'))
                return redirect('transaksi:daftar_transaksi')
    return render(request, 'transaksi/update_transaksi.html', context)

def buat_transaksi(request):
    if request.session.get('user').get('role') != 'Administrator':
        return redirect('home')

    context = {
        'page_title': 'Buat Transaksi'
    }

    form = TransaksiForm
    context['form'] = form

    if request.method == 'POST':
        # Id_transaksi otomatis oleh sistem, +1 ID konsul yang lama
        # Biaya Default = 0
        # status otomatis 'booked'
        form = form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('transaksi:daftar_transaksi')
        else:
            context['form'] = form
            return render(request, 'transaksi/buat_transaksi.html', context)
    
    return render(request, 'transaksi/buat_transaksi.html', context)

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def get_daftar_transaksi():
    with connection.cursor() as cursor:
        cursor.execute('select * from medikago.transaksi')
        list_transaksi = dictfetchall(cursor)
        cursor.close()
    return list_transaksi

def delete_transaksi(request):
    if request.method == 'POST':
        id_transaksi = request.POST.get('id_transaksi')
        if id_transaksi:
            with connection.cursor() as cursor:
                cursor.execute('delete from medikago.transaksi where id_transaksi=%s', [id_transaksi])
                cursor.close()
    return redirect("transaksi:daftar_transaksi")