from django.shortcuts import render, redirect
from django.db import connection
from resep_obat.forms import *

# Create your views here.
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def fetch_obat():
    with connection.cursor() as get_all_obat:
        get_all_obat.execute('select * from medikago.obat')
        result = dictfetchall(get_all_obat)
        get_all_obat.close()
    return result

def fetch_resep():
    with connection.cursor() as get_all_resep:
        query = ('select a.no_resep, a.id_konsultasi, a.total_harga, a.id_transaksi, ')
        query += ("STRING_AGG(c.kode_obat, ', ') kode_obat ")
        query += ('from medikago.resep a, medikago.daftar_obat c ')
        query += ('where a.no_resep = c.no_resep ')
        query += ('group by a.no_resep order by a.no_resep desc')
        get_all_resep.execute(query)
        result = dictfetchall(get_all_resep)
        get_all_resep.close()
    return result

def fetch_daftar_obat(no_resep):
    with connection.cursor() as get_all_daftar:
        query = 'select a.merk_dagang from medikago.obat a'
        query += 'join medikago.daftar_obat b on a.kode = b.kode_obat '
        query += 'where b.no_resep = %s'
        get_all_daftar.execute(query, [no_resep])
        result = dictfetchall(get_all_daftar)
        get_all_daftar.close()
    return result

def check_obat_exists(kode):
    with connection.cursor() as obat:
        obat.execute('select kode from medikago.obat where kode = %s', [kode])
        check_kode = obat.fetchone()

        if check_kode:
            return "kode obat sudah terdaftar"
        
        obat.close()
    
    return None


def daftar_obat(request):
    obat_tuple = fetch_obat()

    context = {
        'page_title': 'Daftar Obat',
        'obat_dict': obat_tuple
    }

    return render(request, 'resep_obat/daftar_obat.html', context)

def buat_obat(request):
    form = CreateObatForm
    context = {
        'page_title': 'Buat Obat',
        'form' : form
    }

    if request.method=='POST':
        form = form(request.POST)
        if form.is_valid():
            kode = request.POST.get('kode')
            stok = request.POST.get('stok')
            harga = request.POST.get('harga')
            komposisi = request.POST.get('komposisi')
            bentuk_sediaan = request.POST.get('bentuk_sediaan')
            merk_dagang = request.POST.get('merk_dagang')

            does_obat_exists = check_obat_exists(kode)

            if does_obat_exists is not None:
                context['error'] = does_obat_exists
                return render(request, 'resep_obat/buat_obat.html', context)
            
            form.save()
            context['form'] = form
            return redirect("resep_obat:daftar_obat")

    return render(request, 'resep_obat/buat_obat.html', context)

def update_obat(request):
    if request.method == "GET":
        return redirect('resep_obat:daftar_obat')

    form = CreateObatForm
    
    context = {
        'page_title': 'Update Obat',
        'form' : form
    }

    if request.method == "POST":
        if request.POST.get('edit') == 'true':
            with connection.cursor() as get_obat:
                get_obat.execute('select * from medikago.obat where kode = %s', [request.POST.get('kode')])
                obat_query = get_obat.fetchone()
                get_obat.close()

            obat = {
                'kode' : obat_query[0],
                'stok' : obat_query[1],
                'harga' : obat_query[2],
                'komposisi' : obat_query[3],
                'bentuk_sediaan' : obat_query[4],
                'merk_dagang' : obat_query[5]
            }

            form = form(obat)
            context['form'] = form
            context['kode'] = obat_query[0]

            return render(request, 'resep_obat/update_obat.html', context)
        else:
            form = form(request.POST)
            if form.is_valid():
                
                kode = request.POST.get('kode')
                stok = request.POST.get('stok')
                harga = request.POST.get('harga')
                komposisi = request.POST.get('komposisi')
                bentuk_sediaan = request.POST.get('bentuk_sediaan')
                merk_dagang = request.POST.get('merk_dagang')

                form.edit(kode=request.POST.get('kode'))
                return redirect('resep_obat:daftar_obat')
    
    return render(request, 'resep_obat/update_obat.html', context)

def delete_obat(request):
    if request.method == 'POST':
        kode = request.POST.get('kode')
        if kode:
            with connection.cursor() as delete_obat:
                delete_obat.execute('delete from medikago.obat where kode = %s', [kode])
                delete_obat.close()
    return redirect("resep_obat:daftar_obat")

def daftar_resep(request):
    resep_tuple = fetch_resep()
    
    print(resep_tuple)
    
    context = {
        'page_title': 'Daftar Resep',
        'resep_dict': resep_tuple
    }
    return render(request, 'resep_obat/daftar_resep.html', context)

def buat_resep(request):

    form = CreateResepObat()

    form.fields['id_konsultasi'].widget.choices = get_id_konsultasi()
    form.fields['id_transaksi'].widget.choices = get_id_transaksi()
    form.fields['daftar_obat'].widget.choices = get_kode_obat()

    context = {
        'page_title': 'Buat Resep',
        'form' : form
    }

    if request.method == "POST":
        form = CreateResepObat(request.POST)
        if form.is_valid():
            id_konsultasi = form.cleaned_data.get('id_konsultasi')
            id_transaksi = form.cleaned_data.get('id_transaksi')
            daftar_obat = form.cleaned_data.get('daftar_obat')

            form.save()
            form.fields['id_konsultasi'].widget.choices = get_id_konsultasi()
            form.fields['id_transaksi'].widget.choices = get_id_transaksi()
            form.fields['daftar_obat'].widget.choices = get_kode_obat()
            
        return redirect("resep_obat:daftar_resep")
    
    return render(request, 'resep_obat/buat_resep.html', context)

def edit_resep(request):
    if request.method == 'GET':
        return redirect("resep_obat:daftar_resep")
        
    form = CreateResepObat

    context = {
        'page_title': 'Edit Resep',
        'form' : form
    }

    if request.method == "POST":
        if request.POST.get('edit') == 'true':
            form = form()
            context['form'] = form
            context['no_resep'] = request.POST.get('no_resep')

            return render(request, 'resep_obat/update_resep.html', context)
        else:
            form = form(request.POST)
            if form.is_valid():

                form.edit(no_resep=request.POST.get('no_resep'))
                return redirect('resep_obat:daftar_resep')
    
    return render(request, 'resep_obat/update_resep.html', context)

def delete_resep(request):
    if request.method == 'POST':
        no_resep = request.POST.get('no_resep')
        if no_resep:
            with connection.cursor() as delete_resep:
                delete_resep.execute('delete from medikago.daftar_obat where no_resep=%s', [no_resep])
                delete_resep.execute('delete from medikago.resep where no_resep=%s', [no_resep])
                delete_resep.close()
    return redirect("resep_obat:daftar_resep")