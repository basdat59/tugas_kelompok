from django import forms
from django.db import connection

class CreateObatForm(forms.Form):
    attrs = {'class' : 'form-control'}

    kode = forms.CharField(label="Kode Obat", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    stok = forms.IntegerField(label="Stok", required=True, min_value=0, widget=forms.NumberInput(attrs=attrs))
    harga = forms.IntegerField(label="Harga", required=True, min_value=0, widget=forms.NumberInput(attrs=attrs))
    komposisi = forms.CharField(label="Komposisi", widget=forms.Textarea(attrs=attrs))
    bentuk_sediaan = forms.CharField(label="Bentuk Sediaan", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    merk_dagang = forms.CharField(label="Merk Dagang", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))

    def is_valid(self):
        valid = super(CreateObatForm, self).is_valid()

        if not valid:
            return valid

        return True

    def save(self, commit=True):
        kode = self.cleaned_data.get('kode')
        stok = self.cleaned_data.get('stok')
        harga = self.cleaned_data.get('harga')
        komposisi = self.cleaned_data.get('komposisi')
        bentuk_sediaan = self.cleaned_data.get('bentuk_sediaan')
        merk_dagang = self.cleaned_data.get('merk_dagang')

        with connection.cursor() as obat:
            obat.execute('insert into medikago.obat values (%s, %s, %s, %s, %s, %s)',
                        [kode, stok, harga, komposisi, bentuk_sediaan, merk_dagang])
            obat.close()
    
    def edit(self, commit=True, kode=None):
        if not kode:
            return
        
        kode = self.cleaned_data.get('kode')
        stok = self.cleaned_data.get('stok')
        harga = self.cleaned_data.get('harga')
        komposisi = self.cleaned_data.get('komposisi')
        bentuk_sediaan = self.cleaned_data.get('bentuk_sediaan')
        merk_dagang = self.cleaned_data.get('merk_dagang')

        with connection.cursor() as save_obat:
            query = 'update medikago.obat '
            query += 'set kode=%s, stok=%s, harga=%s, komposisi=%s, bentuk_sediaan=%s, merk_dagang=%s '
            query += 'where kode=%s'
            save_obat.execute(query, [kode, stok, harga, komposisi, bentuk_sediaan, merk_dagang, kode])
            save_obat.close()


RESEP_CHOICES = [
    ('20d68445-0041-4775-8ef4-713fda501668','713fda501668'),
    ('dddeeeef-85ff-4708-9787-d521aec86cdb', 'd521aec86cdb'),
    ('1933e7f6-1487-4e79-8912-d9cb01a5f8ad', 'd9cb01a5f8ad')
]

TRANSAKSI_CHOICES = [
    ('1','1'),
    ('2','2'),
    ('3','3')
]

OBAT_CHOICES = [
    ('53746-206', '53746-206'),
    ('43598-218', '43598-218'),
    ('21695-671', '21695-671')
]

def get_id_transaksi():
    with connection.cursor() as cursor:
        cursor.execute('select id_transaksi from medikago.transaksi')
        rows = cursor.fetchall()
        cursor.close()
    
    return [tuple((item[0], item[0]))
            for item in rows]
            
def get_id_konsultasi():
    with connection.cursor() as cursor:
        cursor.execute('select id_konsultasi from medikago.sesi_konsultasi')
        rows = cursor.fetchall()
        cursor.close()
    
    return [tuple((item[0], item[0]))
            for item in rows]

def get_kode_obat():
    with connection.cursor() as cursor:
        cursor.execute('select kode from medikago.obat')
        rows = cursor.fetchall()
        cursor.close()
    
    return [tuple((item[0], item[0]))
            for item in rows]

def nomor_resep_generator():
    with connection.cursor() as get_nomor_resep:
        query = "select no_resep from "
        query += "medikago.resep order by "
        query += "nullif(regexp_replace(no_resep, '\D', '', 'g'), '')::int desc limit 1"
        get_nomor_resep.execute(query)
        row = get_nomor_resep.fetchone()
        get_nomor_resep.close()
    
    last_nomor = int(row[0]) if (row is not None) else None
   
    if last_nomor is None:
        return str(1)
    
    return str(last_nomor + 1)

class CreateResepObat(forms.Form):

    
    attrs = {'class' : 'form-control'}

    daftar_transaksi = get_id_transaksi()
    daftar_konsultasi = get_id_konsultasi()
    daftar_kode_obat = get_kode_obat()

    id_konsultasi = forms.ChoiceField(label="ID Konsultasi", required=True, choices=daftar_konsultasi)
    id_transaksi = forms.ChoiceField(label="ID Transaksi", required=True, choices=daftar_transaksi)
    daftar_obat = forms.MultipleChoiceField(label="Daftar Kode Obat", required=True, choices=daftar_kode_obat)

    def save(self, commit=True):
        no_resep = nomor_resep_generator()
        id_konsultasi = self.cleaned_data.get('id_konsultasi')
        total_harga = None
        id_transaksi = self.cleaned_data.get('id_transaksi')
        daftar_obat = self.cleaned_data.get('daftar_obat')

        print(daftar_obat)

        with connection.cursor() as resep:
            resep.execute('insert into medikago.resep values(%s, %s, %s, %s)',
                            [no_resep, id_konsultasi, 0, id_transaksi])
            for obat in daftar_obat:
                resep.execute('insert into medikago.daftar_obat values(%s, %s, %s, %s)',
                            [no_resep, obat, '-', '-'])
            resep.close()
    
    def edit(self, commit=True, no_resep=None):
        if not no_resep:
            return

        id_konsultasi = self.cleaned_data.get('id_konsultasi')
        total_harga = None
        id_transaksi = self.cleaned_data.get('id_transaksi')
        daftar_obat = self.cleaned_data.get('daftar_obat')

        print(daftar_obat)

        with connection.cursor() as resep:
            resep.execute('delete from medikago.daftar_obat where no_resep=%s', [no_resep])
            resep.execute('update medikago.resep set id_konsultasi=%s, id_transaksi=%s, total_harga=%s where no_resep=%s',
                            [id_konsultasi, id_transaksi, 0, no_resep])
            for obat in daftar_obat:
                resep.execute('insert into medikago.daftar_obat values(%s, %s, %s, %s)',
                            [no_resep, obat, '-', '-'])
            resep.close()
