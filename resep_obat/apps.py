from django.apps import AppConfig


class ResepObatConfig(AppConfig):
    name = 'resep_obat'
