from django.urls import path
from resep_obat.views import buat_obat, buat_resep, daftar_obat, daftar_resep, update_obat, delete_obat, delete_resep, edit_resep

app_name = 'resep_obat'

urlpatterns = [
    path('obat/buat/', buat_obat, name='buat_obat'),
    path('obat/daftar/', daftar_obat, name='daftar_obat'),
    path('obat/update', update_obat, name='update_obat'),
    path('obat/delete/', delete_obat, name='delete_obat'),
    path('resep/buat/', buat_resep, name='buat_resep'),
    path('resep/daftar/', daftar_resep, name='daftar_resep'),
    path('resep/delete/', delete_resep, name='delete_resep'),
    path('resep/edit/', edit_resep, name='edit_resep')
]
