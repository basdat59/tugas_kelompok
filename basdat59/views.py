from django.shortcuts import render

def index(request):

    context = {
        'page_title':'Home'
    }

    return render(request, 'basdat59/home.html', context)
    