"""basdat59 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from basdat59.views import index

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='home'),
    path('account/', include("account.urls")),
    path('poliklinik/', include("poliklinik.urls")),
    path('', include('resep_obat.urls')),
    path('rs_cabang/', include('rs_cabang.urls')),
    path('sesi_konsultasi/', include('sesi_konsultasi.urls')),
    path('tindakan/', include('tindakan.urls')),
    path('transaksi/', include('transaksi.urls'))
]
