from django.shortcuts import render

# Create your views here.
def daftar_tindakan(request):
    context = {
        'page_title': 'Daftar Tindakan'
    }
    return render(request, 'tindakan/daftar_tindakan.html', context)

def buat_tindakan(request):
    context = {
        'page_title': 'Buat Tindakan'
    }
    return render(request, 'tindakan/buat_tindakan.html', context)

def daftar_tindakan_poliklinik(request):
    context = {
        'page_title': 'Daftar Tindakan Poliklinik'
    }
    return render(request, 'tindakan/daftar_tindakan_poliklinik.html', context)

def buat_tindakan_poliklinik(request):
    context = {
        'page_title': 'Buat Tindakan Poliklinik'
    }
    return render(request, 'tindakan/buat_tindakan_poliklinik.html', context)