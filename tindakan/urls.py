from django.urls import path
from tindakan.views import buat_tindakan, buat_tindakan_poliklinik, daftar_tindakan, daftar_tindakan_poliklinik

app_name = 'tindakan'

urlpatterns = [
    path('buat/', buat_tindakan, name='buat_tindakan'),
    path('daftar/', daftar_tindakan, name='daftar_tindakan'),
    path('poliklinik/buat/', buat_tindakan_poliklinik, name='buat_tindakan_poliklinik'),
    path('poliklinik/daftar', daftar_tindakan_poliklinik, name='daftar_tindakan_poliklinik')
]
