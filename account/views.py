from collections import namedtuple
from datetime import datetime
from django.db import connection
from django.shortcuts import redirect, render
from account.forms import AdministratorForm, DokterForm, LoginForm, PasienForm
import random


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def check_user_exists(username, email, nomor_identitas):
    with connection.cursor() as check_user:
        # Check Username
        check_user.execute('select * from medikago.pengguna where username=%s', [username])
        check_username = check_user.fetchone()
        if check_username:
            return 'Sudah ada Pengguna dengan Username tersebut!'
        
        # Check email
        check_user.execute('select * from medikago.pengguna where email=%s', [email])
        check_email = check_user.fetchone()
        if check_email:
            return 'Sudah ada Pengguna dengan Email tersebut!'

        # Check nomor id
        check_user.execute('select * from medikago.pengguna where nomor_id=%s', [nomor_identitas])
        check_no_id = check_user.fetchone()
        if check_no_id:
            return 'Sudah ada Pengguna dengan Nomor Identitas tersebut!'

        check_user.close()

    return None


# Nomor_rekam_medis GENERATOR
def nomor_rekam_medis_generator():
    with connection.cursor() as get_no_rekam_medis:
        query = "select no_rekam_medis from "
        query += "medikago.pasien order by "
        query += "nullif(regexp_replace(no_rekam_medis, '\D', '', 'g'), '')::int desc limit 1"
        get_no_rekam_medis.execute(query)
        row = get_no_rekam_medis.fetchone()
        get_no_rekam_medis.close()
    
    last_nomor = int(row[0][3:]) if (row is not None) else None
   
    if last_nomor is None:
        return 'PAS' + str(1)
    
    return 'PAS' + str(last_nomor + 1)


def check_role_pengguna(username):
    with connection.cursor() as check_role:
        # Check admin
        check_role.execute('select * from medikago.administrator where username=%s', [username])
        check_admin = check_role.fetchone()
        if check_admin:
            return {'role': 'Administrator', 'row': check_admin}
        
        # Check Dokter
        check_role.execute('select * from medikago.dokter where username=%s', [username])
        check_dokter = check_role.fetchone()
        if check_dokter:
            return {'role': 'Dokter', 'row': check_dokter}

        # Check Pasien
        check_role.execute('select * from medikago.pasien where username=%s', [username])
        check_pasien = check_role.fetchone()
        if check_pasien:
            return {'role': 'Pasien', 'row': check_pasien}

        check_role.close()
            

# Create your views here.
def register(request):
    context = {
        'page_title': 'Registrasi'
    }
    return render(request, 'account/register.html', context)


def register_admin(request):
    form = AdministratorForm

    context = {
        'page_title':'Registrasi Admin',
        'form':form
    }

    if request.method == "POST":
        form = form(request.POST)
        if form.is_valid():
            # Check apakah username, email, dan nomor id unique atau tidak
            username = request.POST.get('username')
            email = request.POST.get('email')
            nomor_identitas = request.POST.get('nomor_identitas')

            user_check_error = check_user_exists(username, email, nomor_identitas)

            if user_check_error is not None:
                context['error'] = user_check_error
                return render(request, 'account/register_admin.html', context)

            # Kalo ga ada pengguna yang terdaftar, maka daftar dengan save()
            form.save()

            context['message'] = 'Register Success!'
            context['form'] = form
            return render(request, 'account/register_admin.html', context)
            
    return render(request, 'account/register_admin.html', context)


def register_dokter(request):
    form = DokterForm

    context = {
        'page_title':'Registrasi Dokter',
        'form':form
    }

    if request.method == "POST":
        form = form(request.POST)
        if form.is_valid():
            # Check username, email, nomor identitas exists/tidak
            username = request.POST.get('username')
            email = request.POST.get('email')
            nomor_identitas = request.POST.get('nomor_identitas')

            user_check_error = check_user_exists(username, email, nomor_identitas)
            
            if user_check_error is not None:
                context['error'] = user_check_error
                context['form'] = form
                return render(request, 'account/register_dokter.html', context)

            # Kalo ga ada pengguna yang terdaftar, maka daftar dengan save()
            form.save()

            context['message'] = 'Register Success!'
            return render(request, 'account/register_dokter.html', context)

    return render(request, 'account/register_dokter.html', context)


def register_pasien(request):
    form = PasienForm

    context = {
        'page_title': 'Registrasi Pasien',
        'form':form
    }

    if request.method == "POST":
        form = form(request.POST)
        if form.is_valid():
            # Check username, email, nomor identitas exists/tidak
            username = request.POST.get('username')
            email = request.POST.get('email')
            nomor_identitas = request.POST.get('nomor_identitas')

            user_check_error = check_user_exists(username, email, nomor_identitas)

            if user_check_error is not None:
                context['error'] = user_check_error
                context['form'] = form
                return render(request, 'account/register_pasien.html', context)

            # Save user ke database
            # Pengguna Details
            username = form.cleaned_data.get('username')
            # Hash password
            password = form.cleaned_data.get('password')
            nomor_identitas = form.cleaned_data.get('nomor_identitas')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            if form.cleaned_data.get('tanggal_lahir'):
                tanggal_lahir = form.cleaned_data.get('tanggal_lahir').strftime('%Y-%m-%d')
            else:
                tanggal_lahir = None
            email = form.cleaned_data.get('email')
            alamat = form.cleaned_data.get('alamat')

            # Pasien Details
            list_alergi = request.POST.getlist('alergi[]')
            no_rekam_medis = nomor_rekam_medis_generator()

            with connection.cursor() as pengguna:
                pengguna.execute('insert into medikago.pengguna values (%s, %s, %s, %s, %s, %s, %s)',
                                [email, username, password, nama_lengkap, nomor_identitas, tanggal_lahir, alamat])

                pengguna.execute('select nama from medikago.asuransi')
                list_asuransi = pengguna.fetchall()
                random_asuransi = random.choice(list_asuransi)[0]
                
                pengguna.execute('insert into medikago.pasien values (%s, %s, %s)',
                                [no_rekam_medis, username, random_asuransi])

                for alergi in list_alergi:
                    pengguna.execute('insert into medikago.alergi_pasien values (%s, %s)',
                                [no_rekam_medis, alergi])
                
                pengguna.close()

            context['message'] = 'Register Success!'
            return render(request, 'account/register_pasien.html', context)
    
    return render(request, 'account/register_pasien.html', context)


def login(request):
    form = LoginForm

    context = {
        'page_title': 'Login',
        'form':form,
    }

    if request.method == "POST":
        form = form(request.POST)
        if form.is_valid:
            username = request.POST.get('username')
            password = request.POST.get('password')

            with connection.cursor() as get_pengguna:
                get_pengguna.execute('select * from medikago.pengguna where username=%s and password=%s',
                    [username, password]
                )
                pengguna = get_pengguna.fetchone()
                get_pengguna.close()

            if pengguna is None:
                context['error'] = 'Username atau Password salah!'
                context['form'] = form
                return render(request, 'account/login.html', context)

            # Check pengguna ada di tabel apa     
            user_obj = check_role_pengguna(username)
            # Get role (jenis pengguna) dan row (baris pada tabel jenis pengguna)
            role = user_obj.get('role')
            additional_user_details = user_obj.get('row')
            print(additional_user_details)

            # Parse pengguna to dictionary
            user = {
                'role': role,
                'email': pengguna[0],
                'username': pengguna[1],
                'password': pengguna[2],
                'nama_lengkap': pengguna[3],
                'nomor_identitas': pengguna[4],
                'tanggal_lahir': pengguna[5].strftime("%m/%d/%Y"),
                'alamat': pengguna[6]
            }

            if role == "Administrator":
                user['nomor_pegawai'] = additional_user_details[0]
                user['kode_rs'] = additional_user_details[2]
            
            if role == "Dokter":
                user['id_dokter'] = additional_user_details[0]
                user['no_sip'] = additional_user_details[2]
                user['spesialisasi'] = additional_user_details[3]

            if role == "Pasien":
                user['no_rekam_medis'] = additional_user_details[0]
                user['nama_asuransi'] = additional_user_details[2]

                # Get User alergies
                with connection.cursor() as cursor:
                    cursor.execute('select * from medikago.alergi_pasien where no_rekam_medis=%s',
                        [additional_user_details[0]]
                    )
                    alergi_tuple = namedtuplefetchall(cursor)
                user['alergies'] = alergi_tuple

            request.session['user'] = user
            
            return redirect('account:profil')

    return render(request, 'account/login.html', context)


def logout(request):
    request.session.flush()
    return redirect('home')


def profil(request):
    context = {
        'page_title': 'Profil',
    }
    return render(request, 'account/profile.html', context)
