from django.urls import path, include
from account.views import login, logout, profil, register, register_admin, register_dokter, register_pasien

app_name = 'account'

urlpatterns = [
    path('register/', register, name='register'),
    path('register/admin/', register_admin, name='register_admin'),
    path('register/dokter/', register_dokter, name='register_dokter'),
    path('register/pasien/', register_pasien, name='register_pasien'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('profil/', profil, name='profil')
]