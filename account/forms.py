import string
import random

from datetime import datetime
from django import forms
from django.db import connection


year_choice = ["{0}".format(year) for year in range(1995, 2031)]


# NOMOR_PEGAWAI GENERATOR LOCAL
def nomor_pegawai_generator():
    with connection.cursor() as get_nomor_pegawai:
        query = "select nomor_pegawai from "
        query += "medikago.administrator order by "
        query += "nullif(regexp_replace(nomor_pegawai, '\D', '', 'g'), '')::int desc limit 1"
        get_nomor_pegawai.execute(query)
        row = get_nomor_pegawai.fetchone()
        get_nomor_pegawai.close()
    
    last_nomor = int(row[0][3:]) if (row is not None) else None
   
    if last_nomor is None:
        return 'ADM' + str(1)
    
    return 'ADM' + str(last_nomor + 1)


def id_dokter_generator():
    with connection.cursor() as get_id_dokter:
        query = "select id_dokter from "
        query += "medikago.dokter order by "
        query += "nullif(regexp_replace(id_dokter, '\D', '', 'g'), '')::int desc limit 1"
        get_id_dokter.execute(query)
        row = get_id_dokter.fetchone()
        get_id_dokter.close()

    last_nomor = int(row[0][3:]) if (row is not None) else None
    
    if last_nomor is None:
        return 'DOK' + str(1)
    
    return 'DOK' + str(last_nomor + 1)


class LoginForm(forms.Form):
    attrs = {'class': 'form-control'}

    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(attrs=attrs))


class AdministratorForm(forms.Form):
    attrs = {'class': 'form-control'}

    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(attrs=attrs))
    nomor_identitas = forms.CharField(label='Nomor identitas', required=True, max_length=50,
                                      widget=forms.TextInput(attrs=attrs))
    nama_lengkap = forms.CharField(label='Nama lengkap', required=True, max_length=50,
                                   widget=forms.TextInput(attrs=attrs))
    tanggal_lahir = forms.DateField(label='Tanggal lahir', required=False,
                                    widget=forms.SelectDateWidget(attrs=attrs, years=year_choice))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.EmailInput(attrs=attrs))
    alamat = forms.CharField(label='Alamat', required=False, widget=forms.Textarea(attrs=attrs))

    def save(self, commit=True):
        # Pengguna Details
        username = self.cleaned_data.get('username')
        # Hash password
        password = self.cleaned_data.get('password')
        nomor_identitas = self.cleaned_data.get('nomor_identitas')
        nama_lengkap = self.cleaned_data.get('nama_lengkap')
        if self.cleaned_data.get('tanggal_lahir'):
            tanggal_lahir = self.cleaned_data.get('tanggal_lahir').strftime('%Y-%m-%d')
        else:
            tanggal_lahir = None
        email = self.cleaned_data.get('email')
        alamat = self.cleaned_data.get('alamat')

        # Administrator details
        nomor_pegawai = nomor_pegawai_generator() # For Heroku
        kode_rs = "RS1" # HEROKU : RS1

        # Save to the database postgresql
        with connection.cursor() as pengguna:
            pengguna.execute('insert into medikago.pengguna values (%s, %s, %s, %s, %s, %s, %s)',
                             [email, username, password, nama_lengkap, nomor_identitas, tanggal_lahir, alamat])
            pengguna.execute('insert into medikago.administrator values (%s, %s, %s)',
                             [nomor_pegawai, username, kode_rs])
            pengguna.close()


class DokterForm(forms.Form):
    attrs = {'class': 'form-control'}

    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(attrs=attrs))
    nomor_identitas = forms.CharField(label='Nomor identitas', required=True, max_length=50,
                                      widget=forms.TextInput(attrs=attrs))
    nama_lengkap = forms.CharField(label='Nama lengkap', required=True, max_length=50,
                                   widget=forms.TextInput(attrs=attrs))
    tanggal_lahir = forms.DateField(label='Tanggal lahir', required=False,
                                    widget=forms.SelectDateWidget(attrs=attrs, years=year_choice))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.EmailInput(attrs=attrs))
    alamat = forms.CharField(label='Alamat', required=False, widget=forms.Textarea(attrs=attrs))
    no_sip = forms.CharField(label='No SIP', required=True, widget=forms.TextInput(attrs=attrs))
    spesialisasi = forms.CharField(label='Spesialisasi', required=False, widget=forms.Textarea(attrs=attrs))

    def save(self, commit=True):
        # Pengguna Details
        username = self.cleaned_data.get('username')
        # Hash password
        password = self.cleaned_data.get('password')
        nomor_identitas = self.cleaned_data.get('nomor_identitas')
        nama_lengkap = self.cleaned_data.get('nama_lengkap')
        if self.cleaned_data.get('tanggal_lahir'):
            tanggal_lahir = self.cleaned_data.get('tanggal_lahir').strftime('%Y-%m-%d')
        else:
            tanggal_lahir = None
        email = self.cleaned_data.get('email')
        alamat = self.cleaned_data.get('alamat')

        # Dokter details
        id_dokter = id_dokter_generator() # For Heroku
        no_sip = self.cleaned_data.get('no_sip')
        spesialisasi = self.cleaned_data.get('spesialisasi')

        with connection.cursor() as pengguna:
            pengguna.execute('insert into medikago.pengguna values (%s, %s, %s, %s, %s, %s, %s)',
                             [email, username, password, nama_lengkap, nomor_identitas, tanggal_lahir, alamat])
            pengguna.execute('insert into medikago.dokter values (%s, %s, %s, %s)',
                             [id_dokter, username, no_sip, spesialisasi])
            pengguna.close()


class PasienForm(forms.Form):
    attrs = {'class': 'form-control'}

    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(attrs=attrs))
    nomor_identitas = forms.CharField(label='Nomor identitas', required=True, max_length=50,
                                      widget=forms.TextInput(attrs=attrs))
    nama_lengkap = forms.CharField(label='Nama lengkap', required=True, max_length=50,
                                   widget=forms.TextInput(attrs=attrs))
    tanggal_lahir = forms.DateField(label='Tanggal lahir', required=False,
                                    widget=forms.SelectDateWidget(attrs=attrs, years=year_choice))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.EmailInput(attrs=attrs))
    alamat = forms.CharField(label='Alamat', required=False, widget=forms.Textarea(attrs=attrs))
